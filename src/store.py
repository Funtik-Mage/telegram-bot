import ast
from wiki_searcher import get_topic_summary, find_wiki_page

class AppStore:
    prompt_list = []
    current_text: str | None = None

    current_task: str | None = None
    current_topic: str | None = None
    topics_queue = []
    topic_index = 0

    def remove_topics_with_no_info(self):
        wikipedia_pages = []

        for topic_name in self.topics_queue:
            wiki_page = find_wiki_page(topic_name)
            if wikipedia_pages is not None:
                wikipedia_pages.append(wiki_page)

        self.topics_queue = wikipedia_pages

    def set_task_queue(self, task_queue: str):
        self.topic_index = 0
        self.topics_queue = ast.literal_eval(task_queue)
        self.current_topic = self.topics_queue[self.topic_index]
        self.remove_topics_with_no_info()

        return self.topics_queue

    def set_text_for_current_topic(self):
        if self.current_topic is None:
            return None

        self.current_text = get_topic_summary(self.current_topic)
        return self.current_text

    def has_next_task(self):
        return len(self.topics_queue) > 0 and self.topic_index < len(self.topics_queue) - 1

    def has_prev_task(self):
        return len(self.topics_queue) > 0 and self.topic_index > 0

    def next_task(self):
        if not self.has_next_task():
            return None
        
        self.topic_index += 1
        self.current_topic = self.topics_queue[self.topic_index]

        return self.current_text

    def prev_task(self):
        if not self.has_prev_task():
            return None
        
        self.topic_index -= 1
        self.current_topic = self.topics_queue[self.topic_index]

        return self.current_text

    def clear(self):
        self.prompt_list.clear()
        self.current_topic = None
        self.current_text = None
        self.topics_queue.clear()

    def clear_prompts(self):
        self.prompt_list.clear()

app_store = AppStore()


INTERMEDIATE, DESCRIPTION,  DESCRIPTION_MORE_TASKS, TASK, TASK_DESCRIPTION, = range(5)
