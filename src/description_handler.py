from telegram import ReplyKeyboardMarkup, Update
from telegram.ext import (
    ContextTypes,
    ConversationHandler,
)
from prompts import generate_tasks, generate_more_tasks
from store import DESCRIPTION, DESCRIPTION_MORE_TASKS, TASK_DESCRIPTION, INTERMEDIATE, app_store
from task_bot_handler import task_description_handler

SOMETHING_WENT_WRONG_ANSWER = "К соожалению, что-то пошло не так. Попробуйте в другой раз."
MORE_TASKS_ANSWER = """
Хотите, чтобы я сгенерировал еще заданий? Выберите тип задания, либо нажмите "Завершить", \
чтобы добавить новый текст.
"""

def get_buttons_for_description():
    queue_buttons = []
    if app_store.has_prev_task():
        queue_buttons.append("Предыдущая тема")
    if app_store.has_next_task():
        queue_buttons.append("Следующая тема")

    return [
        queue_buttons,
        ["Первый тип", "Второй тип", "Завершить"]
    ]

async def description_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if not update.message:
        return ConversationHandler.END

    if update.message.text == "Предыдущая тема":
        return await task_description_handler(update, context)

    if update.message.text == "Следующая тема":
        return await task_description_handler(update, context)

    base_buttons = get_buttons_for_description()
    base_buttons.append(["Сгенерировать еще раз"])

    reply_markup = ReplyKeyboardMarkup(
        base_buttons,
        one_time_keyboard=True,
        resize_keyboard=True
    )

    if update.message.text == "Да" or update.message.text == "Сгенерировать еще раз":
        response = generate_tasks()

        if response is None:
            await update.message.reply_text(SOMETHING_WENT_WRONG_ANSWER)
            return INTERMEDIATE

        await update.message.reply_text(
            response,
        )
        await update.message.reply_text(
            MORE_TASKS_ANSWER,
            reply_markup=reply_markup,
        )
        return DESCRIPTION_MORE_TASKS
    elif update.message.text == "Нет":
        return INTERMEDIATE


async def description_more_tasks_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if not update.message:
        return ConversationHandler.END

    if update.message.text == "Сгенерировать еще раз":
        return await description_handler(update, context)

    if update.message.text == "Предыдущая тема":
        return await task_description_handler(update, context)

    if update.message.text == "Следующая тема":
        return await task_description_handler(update, context)

    if update.message.text == "Завершить":
        await update.message.reply_text(
            "Завершено.",
        )
        return INTERMEDIATE
    else:
        reply_markup = ReplyKeyboardMarkup(
            get_buttons_for_description(),
            one_time_keyboard=True,
            resize_keyboard=True
        )
        response = generate_more_tasks("first" if update.message.text == "Первый тип" else "second")
        await update.message.reply_text(
            response,
        )
        await update.message.reply_text(
            MORE_TASKS_ANSWER,
            reply_markup=reply_markup,
        )
        return DESCRIPTION_MORE_TASKS
