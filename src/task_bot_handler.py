from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)
from prompts import get_topics_from_task
from store import TASK, TASK_DESCRIPTION, DESCRIPTION, INTERMEDIATE, app_store

SOMETHING_WENT_WRONG_ANSWER = "К соожалению, что-то пошло не так. Попробуйте в другой раз."
FIND_TEXT_FOR_TOPIC = """
Могу попытаться найти текст для каждой из тем, которые вы указали. Начать? \
"""
NOT_FOUND_TOPICS = """
Не удалось найти темы из указанного задания. Попробуйте ввести другое задание.
"""
FOUND_TOPICS_TEXT = """
По моему мнению, в задании были затронуты затронуты следующие темы\
"""

def get_buttons_for_task_description():
    queue_buttons = []
    if app_store.has_prev_task():
        queue_buttons.append("Предыдущая тема")
    if app_store.has_next_task():
        queue_buttons.append("Следующая тема")

    return [
        queue_buttons,
        ["Да", "Нет"]
    ]

async def task_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if not update.message:
        return ConversationHandler.END

    reply_markup = ReplyKeyboardMarkup(
        [["Да", "Нет"]],
        one_time_keyboard=True,
        resize_keyboard=True
    )

    if update.message.text == "Да":
        response = get_topics_from_task(app_store.current_task)

        if response is None:
            await update.message.reply_text(SOMETHING_WENT_WRONG_ANSWER)
            return INTERMEDIATE

        topics = app_store.set_task_queue(response)
        if len(topics) == 0:
            await update.message.reply_text(NOT_FOUND_TOPICS)
            return INTERMEDIATE

        await update.message.reply_text(
            f"{FOUND_TOPICS_TEXT}:\n{str(topics)}",
        )
        await update.message.reply_text(
            FIND_TEXT_FOR_TOPIC,
            reply_markup=reply_markup,
        )
        return TASK_DESCRIPTION
    elif update.message.text == "Нет":
        return INTERMEDIATE

async def task_description_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if not update.message:
        return ConversationHandler.END

    reply_markup = ReplyKeyboardMarkup(
        get_buttons_for_task_description(),
        one_time_keyboard=True,
        resize_keyboard=True
    )

    if update.message.text == "Предыдущая тема":
        app_store.prev_task()

    if update.message.text == "Следующая тема":
        app_store.next_task()

    trigger_message = ["Да", "Предыдущая тема", "Следующая тема"]
    if trigger_message.count(update.message.text if update.message.text else "") > 0:
        text = app_store.set_text_for_current_topic()
        if text is None:
            await update.message.reply_text("Не удалось найти текст по теме.")
            return INTERMEDIATE

        await update.message.reply_text(
            f"Все темы: {str(app_store.topics_queue)}\nКраткое содержание по теме {app_store.current_topic}:\n{text}",
        )
        await update.message.reply_text(
            "Могу попробовать составить вопросы по тексту. Начать?",
            reply_markup=reply_markup,
        )
        return DESCRIPTION
    elif update.message.text == "Нет":
        return INTERMEDIATE
