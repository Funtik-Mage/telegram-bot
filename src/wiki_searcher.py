import wikipediaapi
import requests

S = requests.Session()
wiki_wiki = wikipediaapi.Wikipedia('ru')

URL = "https://ru.wikipedia.org/w/api.php"

def find_wiki_page(topic_name: str):
    SEARCH_PARAMS = {
        "action": "query",
        "list": "search",
        "format": "json",
        "srsearch": topic_name
    }

    R = S.get(url=URL, params=SEARCH_PARAMS)
    DATA = R.json()

    if len(DATA["query"]["search"]) == 0:
        return None
    
    return DATA["query"]["search"][0]["title"]


def get_topic_summary(topic_name: str):
    page = find_wiki_page(topic_name)
    if not page:
        return None
    
    page_py = wiki_wiki.page(page)
    return page_py.summary
