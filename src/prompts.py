from logging import log
import os
import tomllib
from typing import Any, Literal, Optional, TypedDict, Unpack
from constants import PROMPTS_DIR_PATH
import openai
import json
from store import app_store
from logger import logger

openai.api_key = os.environ["OPENAI_API_TOKEN"]
PromptType = Literal["check_text_type"]

class TypedPromptData(TypedDict):
    role: str
    request: str


def chatgpt_prompt(func):
    prompt_file_name = func.__name__ + ".toml"
    prompt_file_path = PROMPTS_DIR_PATH / prompt_file_name

    content = None
    with open(prompt_file_path, "rb") as f:
        content = tomllib.load(f)

    def wrapper_func(*args, **kwargs):
        text = args[0] if len(args) > 0 else None
        return func(text, **kwargs, content=content)

    return wrapper_func

def get_request_text(request: str, content: str):
    return f"{request} \n\"\"\"\n{content}\n\"\"\""


class CheckTextResponse(TypedDict):
    type: Literal["description", "task", "unknown"]
    subject: Optional[Literal["history"]]

@chatgpt_prompt
def check_text_type(text: str, **context) -> None | CheckTextResponse:
    app_store.clear()
    prompt_data: TypedPromptData | None = context["content"]

    if prompt_data is None:
        return None

    request_text = get_request_text(prompt_data["request"], text)
    app_store.current_text = text

    chatgpt_response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": prompt_data["role"]},
            {"role": "user", "content": request_text},
        ],
        temperature=0,
    )

    logger.info(chatgpt_response)
    response = chatgpt_response.choices[0].message # type: ignore

    return json.loads(response.content)

@chatgpt_prompt
def generate_tasks(*args, **context) -> None | str:
    app_store.clear_prompts()
    prompt_data: TypedPromptData | None = context["content"]

    if prompt_data is None or app_store.current_text is None:
        return None

    request_text = get_request_text(prompt_data["request"], app_store.current_text)
    app_store.prompt_list.extend([
            {"role": "system", "content": prompt_data["role"]},
            {"role": "user", "content": request_text},
        ]
    )

    chatgpt_response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=app_store.prompt_list,
        temperature=0.5,
        top_p=0.3,
    )
    logger.info(chatgpt_response)

    response = chatgpt_response.choices[0].message # type: ignore

    if response is not None:
        app_store.prompt_list.append(response)

    return response.content


def generate_more_tasks(task_type: Literal["first", "second"]):
    local_prompt_list = app_store.prompt_list.copy()
    prompt_content = None

    if task_type == "first":
        prompt_content = "Составь больше заданий первого типа, если возможно"
    else:
        prompt_content = "Составь больше заданий второго типа, если возможно"

    local_prompt_list.append({"role": "user", "content": prompt_content})

    chatgpt_response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=local_prompt_list,
        temperature=0.5,
        top_p=0.3,
    )
    logger.info(chatgpt_response)

    response = chatgpt_response.choices[0].message # type: ignore
    return response.content

@chatgpt_prompt
def get_topics_from_task(text, **context):
    prompt_data: TypedPromptData | None = context["content"]

    if prompt_data is None or text is None:
        return None

    request_text = get_request_text(prompt_data["request"], text)
    chatgpt_response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": prompt_data["role"]},
            {"role": "user", "content": request_text},
        ],
        temperature=0.7,
    )
    logger.info(chatgpt_response)

    response = chatgpt_response.choices[0].message # type: ignore
    return response.content

