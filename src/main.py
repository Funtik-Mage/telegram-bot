#!/usr/bin/env python3

from dotenv.main import load_dotenv
load_dotenv()

import os
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

from logger import logger
from main_bot_handler import main_handler
from description_handler import description_handler, description_more_tasks_handler
from task_bot_handler import task_handler, task_description_handler
from store import DESCRIPTION_MORE_TASKS, DESCRIPTION, TASK, TASK_DESCRIPTION, INTERMEDIATE

bot_token = os.environ['BOT_TOKEN']

BOT_INFO_ANSWER = """
Я умею составлять вопросы по текстам на историческую тему. \
Помимо этого, если вы приведете задания из ЕГЭ по истории, \
я смогу определить темы, затронутые в задании и предложить возможные \
вопросы по данным темам.
"""

async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Cancels and ends the conversation."""
    if not update.message:
        return ConversationHandler.END

    await update.message.reply_text(BOT_INFO_ANSWER)
    return INTERMEDIATE


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(bot_token).build()

    conv_handler = ConversationHandler(
        entry_points=[MessageHandler(filters.TEXT, main_handler)],
        states={
            INTERMEDIATE: [MessageHandler(filters.TEXT, main_handler)],
            DESCRIPTION: [MessageHandler(
                filters.Regex("^(Да|Нет|Предыдущая тема|Следующая тема)$"),
                description_handler
            )],
            DESCRIPTION_MORE_TASKS: [MessageHandler(
                filters.Regex("^(Первый тип|Второй тип|Завершить|Предыдущая тема|Следующая тема|Сгенерировать еще раз)$"),
                description_more_tasks_handler
            )],
            TASK: [MessageHandler(
                filters.Regex("^(Да|Нет)$"),
                task_handler
            )],
            TASK_DESCRIPTION: [MessageHandler(
                filters.Regex("^(Да|Нет|Предыдущая тема|Следующая тема)$"),
                task_description_handler
            )],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    application.add_handler(conv_handler)

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()
