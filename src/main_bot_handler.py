from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)
from prompts import check_text_type
from logger import logger
from store import DESCRIPTION, TASK, INTERMEDIATE
from store import app_store


SOMETHING_WENT_WRONG_ANSWER = "К соожалению, что-то пошло не так. Попробуйте в другой раз."
UNKNOWN_TYPE_ANSWER = """
К сожалению, я не смогу сделать ничего полезного с приведенным текстом. \
Я умею составлять задания и вопросы по текстам на историческую тему. \
Помимо этого, если вы приведете задания из ЕГЭ по истории, \
я смогу определить темы, затронутые в задании, и предложить возможные \
вопросы по данным темам.
"""
DESCRIPTION_TYPE_ANSWER = """
Похоже, что это описание. Хотите, чтобы я составил вопросы по тексту?
"""
TASK_TYPE_ANSWER = """
Похоже, что это задание. Хотите, чтобы я определил темы, \
затронутые в задании?
"""

async def main_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    app_store.clear()
    if not update.message or not update.message.text:
        return ConversationHandler.END

    response = check_text_type(update.message.text)
    logger.info(response)

    if response is None:
        await update.message.reply_text(SOMETHING_WENT_WRONG_ANSWER)
    else:
        reply_markup = ReplyKeyboardMarkup(
            [["Да", "Нет"]],
            one_time_keyboard=True,
            resize_keyboard=True
        )
        if response.get("type") == "description":
            await update.message.reply_text(
                DESCRIPTION_TYPE_ANSWER,
                reply_markup=reply_markup,
            )
            return DESCRIPTION
        elif response.get("type") == "task":
            await update.message.reply_text(
                TASK_TYPE_ANSWER,
                reply_markup=reply_markup,
            )
            app_store.current_task = update.message.text
            return TASK
        else:
            await update.message.reply_text(UNKNOWN_TYPE_ANSWER)

    return INTERMEDIATE
