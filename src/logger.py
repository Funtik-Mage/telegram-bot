import logging
from constants import PROJECT_DIR

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
    filename=PROJECT_DIR / "bot.log",
    filemode="a",
)

logger = logging.getLogger(__name__)

